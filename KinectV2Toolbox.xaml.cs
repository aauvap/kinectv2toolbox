﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Aalborg University">
// </copyright>
//------------------------------------------------------------------------------


namespace KinectV2Toolbox
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.IO.Compression;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Media3D;
    using System.Windows.Media.Imaging;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.Drawing;
    using Microsoft.Kinect;
    using System.Collections;
    using System.Text;
    using Emgu.CV;
    using Emgu.Util;
    using Emgu.CV.Structure;
    using Emgu.CV.CvEnum;



    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Base directory where the Depth png-images reside
        /// </summary>
        private string baseFolder;

        /// <summary>
        /// Base directory where the Lookup png-images reside
        /// </summary>
        private string lookupBaseFolder;

        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Size of the RGB pixel in the bitmap
        /// </summary>
        private readonly int bytesPerPixel = (PixelFormats.Bgr24.BitsPerPixel + 7) / 8;

        /// <summary>
        /// Storage for receiving depth frame data from disk
        /// </summary>
        private ushort[] depthFrameData = null;

        /// <summary>
        /// Storage for recieving color frame data from disk
        /// </summary>
        private byte[] colorFrameData = null;

        /// <summary>
        /// Intermediate storage for the depth to color mapping
        /// </summary>
        private ColorSpacePoint[] colorPoints = null;

        /// <summary>
        /// Intermediate storage for the depth to camera mapping
        /// </summary>
        private CameraSpacePoint[] cameraPoints = null;

        /// <summary>
        /// Intermediate storage for the color to depth mapping
        /// </summary>
        private DepthSpacePoint[] depthPoints = null;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Offline coordinate mappers retrieved from disk
        /// </summary>
        List<ushort[]> offlineDepthToRgbMapperX;
        List<ushort[]> offlineDepthToRgbMapperY;
        List<ushort[]> offlineRgbToDepthMapperX;
        List<ushort[]> offlineRgbToDepthMapperY;
        Microsoft.Kinect.PointF[] offlineDepthToCameraMapper;


        /// <summary>
        /// List of point clouds to be written to disk
        /// </summary>
        private ArrayList clouds = new ArrayList();

        /// <summary>
        /// Size of the depth pixel in the bitmap
        /// </summary>
        private readonly int bytesPrDepthPixel = (PixelFormats.Gray16.BitsPerPixel + 7) / 8;

        /// <summary>
        /// Flag deciding if we include color information in the point cloud
        /// </summary>
        bool IncludeColor;

        /// <summary>
        /// Flag deciding if we use the connected Kinect Sensor
        /// </summary>
        bool useConnectedKinectForRegistration;

        int depthWidth;
        int depthHeight;
        int colorWidth;
        int colorHeight;

        private int selectedTabIndex;

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            // use the window object as the view model
            this.DataContext = this;



            // initialize the components (controls) of the window
            this.InitializeComponent();



            depthWidth = 512;
            depthHeight = 424;
            colorWidth = 1920;
            colorHeight = 1080;

            // allocate space to put the pixels being received and converted. 
            // Hard coded as this must work without a sensor connected
            this.depthFrameData = new ushort[depthWidth * depthHeight];
            this.cameraPoints = new CameraSpacePoint[depthWidth * depthHeight];
            this.depthPoints = new DepthSpacePoint[colorWidth * colorHeight];
            this.colorPoints = new ColorSpacePoint[depthWidth * depthHeight];
            this.colorFrameData = new byte[colorWidth * colorHeight * bytesPerPixel];

            // Initialize the base folder
            this.baseFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + "\\Capture";
            this.pathBox.Text = baseFolder;
            this.regPathBox.Text = baseFolder;
            this.lookupBaseFolder = this.baseFolder + "\\Lookup";
            this.lookupPathBox.Text = lookupBaseFolder;
            this.regLookupPathBox.Text = lookupBaseFolder;

            offlineDepthToRgbMapperX = new List<ushort[]>();
            offlineDepthToRgbMapperY = new List<ushort[]>();
            offlineRgbToDepthMapperX = new List<ushort[]>();
            offlineRgbToDepthMapperY = new List<ushort[]>();

            this.IncludeColor = false;
            this.useConnectedKinectForRegistration = false;
        }

        private bool openKinectSensor()
        {
            bool activeSensor = false;
            bool escape = false;

            while (!activeSensor && !escape)
            {
                // get the kinectSensor object
                this.kinectSensor = KinectSensor.GetDefault();

                // get the coordinate mapper
                this.coordinateMapper = this.kinectSensor.CoordinateMapper;

                // open the sensor
                this.kinectSensor.Open();

                if (this.kinectSensor.IsAvailable)
                {
                    activeSensor = true;
                    return true;
                } else
                {
                    MessageBoxResult result = System.Windows.MessageBox.Show("Connect a Kinect for Windows v2 sensor and press OK", 
                        "No Kinect sensor connected", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.Cancel)
                    {
                        escape = true;
                        return false;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>
        /// Gets or sets the current status text to display
        /// </summary>
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }


        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (useConnectedKinectForRegistration)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }

        }

        private int readDepthImage(string filepath)
        {
            try
            {
                // Open a stream to decode an png image
                Stream depthStreamSource = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.Read);
                PngBitmapDecoder depthDecoder = new PngBitmapDecoder(depthStreamSource, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                BitmapSource depthBitmap = depthDecoder.Frames[0];

                depthBitmap.CopyPixels(depthFrameData, depthWidth * bytesPrDepthPixel, 0);
            }
            catch (Exception)
            {
                Dispatcher.Invoke(delegate()
                {
                    this.StatusText = Path.GetFileName(filepath) + " is not a proper depth image";
                });
                return 1;
            }
            
            return 0;
        }

        private int readColorImage(string colorBaseFolder, string filepath)
        {
            string colorFilepath;

            try
            {
                // Open a stream to decode an jpg image
                string baseFilename = Path.GetFileName(filepath);
                string colorFilename = baseFilename.Replace(".png", ".jpg").Replace("D", "RGB");

                colorFilepath = colorBaseFolder + colorFilename;

                Stream colorStreamSource = new FileStream(colorFilepath, FileMode.Open, FileAccess.Read, FileShare.Read);
                JpegBitmapDecoder colorDecoder = new JpegBitmapDecoder(colorStreamSource, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                BitmapSource colorBitmap = colorDecoder.Frames[0];

                colorBitmap.CopyPixels(colorFrameData, colorWidth * bytesPerPixel, 0);
            }
            catch (Exception)
            {
                Dispatcher.Invoke(delegate()
                {
                    this.StatusText = Path.GetFileName(filepath).Replace(".png", ".jpg").Replace("D", "RGB") + " does not exist";
                });
                return 1;
            }

            return 0;

        }

        private void convertImageToPolygon()
        {
            StringBuilder sb = new StringBuilder();
            int nbrPoints = 0;

            // Convert depth every depth pixel to world coordinates
            if (useConnectedKinectForRegistration)
            {
                this.coordinateMapper.MapDepthFrameToCameraSpace(depthFrameData, cameraPoints);

                if (IncludeColor)
                {
                    this.coordinateMapper.MapDepthFrameToColorSpace(this.depthFrameData, colorPoints);
                }
            }
            else 
            {
                // Manually map the depth frame to camera space / world coordinates
                int count = 0; 
                foreach (ushort pixel in depthFrameData)
                {
                    float pixelF = (float)pixel;

                    if (pixel >= 500  && pixel < 10000)
                    {
                    cameraPoints[count].X = offlineDepthToCameraMapper[count].X * pixelF/1000;
                    cameraPoints[count].Y = offlineDepthToCameraMapper[count].Y * pixelF/1000;
                    cameraPoints[count].Z = pixelF/1000;
                    }
                    else {
                        // We have an invalid point
                        cameraPoints[count].X = Single.NegativeInfinity;
                        cameraPoints[count].Y = Single.NegativeInfinity;
                        cameraPoints[count].Z = Single.NegativeInfinity;
                    }
                    count++;
                }

                if (IncludeColor)
                {
                    // Manually map the depth frame to color space
                    count = 0;
                    foreach (ushort pixel in depthFrameData)
                    {
                        if (pixel >= 500 && pixel < 8000)
                        {
                            colorPoints[count].X = offlineDepthToRgbMapperX[pixel - 500][count];
                            colorPoints[count].Y = offlineDepthToRgbMapperY[pixel - 500][count];
                        } else{
                            colorPoints[count].X = Single.NegativeInfinity;
                            colorPoints[count].Y = Single.NegativeInfinity;
                        }
                        count++;
                    }
                }
            }

            
            // Loop thorugh every point to add it to the ply file stream
            for (int y = 0; y < depthHeight; ++y)
            {
                for (int x = 0; x < depthWidth; ++x)
                {
                    // Calculate index into the depth array
                    int depthIndex = (y * depthWidth) + x;
                    CameraSpacePoint p = cameraPoints[depthIndex];

                    byte r = 0; byte g = 0; byte b = 0;

                    if (IncludeColor)
                    {
                        ColorSpacePoint colorPoint = this.colorPoints[depthIndex];

                        // We have to make sure that we map to a valid point in the color space
                        int colorX = (int)Math.Floor(colorPoint.X + 0.5);
                        int colorY = (int)Math.Floor(colorPoint.Y + 0.5);
                        if ((colorX >= 0) && (colorX < colorWidth) && (colorY >= 0) && (colorY < colorHeight))
                        {
                            int colorIndex = ((colorY * colorWidth) + colorX) * 3;

                            int displayIndex = depthIndex * this.bytesPerPixel;

                            // Extract the corresponding blue, green, and red pixel values
                            b = this.colorFrameData[colorIndex++];
                            g = this.colorFrameData[colorIndex++];
                            r = this.colorFrameData[colorIndex++];
                        }
                    }
                     
                    if (!(Double.IsInfinity(p.X)) && !(Double.IsInfinity(p.Y)) && !(Double.IsInfinity(p.Z)))
                    {
                        if (IncludeColor)
                        {
                            sb.Append(String.Format(CultureInfo.InvariantCulture, "{0} {1} {2} {3} {4} {5}\n", p.X, p.Y, p.Z, r, g, b));
                        }
                        else
                        {
                            sb.Append(String.Format(CultureInfo.InvariantCulture, "{0} {1} {2}\n", p.X, p.Y, p.Z));
                        }
                        nbrPoints++;
                    }
                }
            }

            // Write the header to the ply file
            String header = "";

            if (!IncludeColor)
            {
                header = "ply \n" +
                    "format ascii 1.0 \n" +
                    "element vertex " + nbrPoints + "\n" +
                    "property float x \n" +
                    "property float y \n" +
                    "property float z \n" +
                    "end_header \n";
            }
            else
            {
                header = "ply \n" +
                    "format ascii 1.0 \n" +
                    "element vertex " + nbrPoints + "\n" +
                    "property float x \n" +
                    "property float y \n" +
                    "property float z \n" +
                    "property uchar red \n" +
                    "property uchar green \n" +
                    "property uchar blue \n" +
                    "end_header \n";
            }

            if (nbrPoints > 0)
            {
                sb.Insert(0, header);
            } else {
                sb.Append(header);
            }
            clouds.Add(sb.ToString());
        }

        private void savePolygons(string baseDirectory, string filename)
        {
            string polygonFilename = Path.GetFileName(filename.Replace(".png", ".ply"));
            string polygonPath = baseDirectory + "\\" + polygonFilename;

            using (StreamWriter file = new StreamWriter(polygonPath))
            {
                file.WriteLine(clouds[0]);
            }
            clouds.Clear();
        }

        private void saveCompressedPolygons(List<string> filenames, int startIndex, int endIndex)
        {
            Dispatcher.Invoke(delegate()
            {
                this.StatusText = "Compressing image " + startIndex.ToString() + " to " + endIndex.ToString();
            });
            
            using (FileStream zipToOpen = new FileStream(this.baseFolder + "\\Polygons.zip", FileMode.OpenOrCreate))
            {
                using (ZipArchive zipArchive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                {
                    for (int i = 0; i < filenames.Count; ++i)
                    {
                        string polygonFilename = Path.GetFileName(filenames[i].Replace(".png", ".ply"));
                        ZipArchiveEntry zipEntry = zipArchive.CreateEntry(polygonFilename);

                        using (StreamWriter writer = new StreamWriter(zipEntry.Open()))
                        {
                            writer.WriteLine(clouds[i]);
                        }
                    }
                }
            }

            clouds.Clear();

        }

        private void processImages(bool enableZipFolders)
        {
            // Get all .png-files in current directory (depth images)
            string[] depthFilePaths = Directory.GetFiles(this.baseFolder + "\\D", "*.png");
            string colorBaseFolder = this.baseFolder + "\\RGB\\";

            Dispatcher.Invoke(delegate()
            {
                this.progressBar.Visibility = Visibility.Visible;                
            });

            int batchSize = 20;
            int convertedImages = 0;
            List<string> currentDepthFiles = new List<string>();
            List<string> currentColorFiles = new List<string>();

            // If zip files are toggled off, create a new directory for the polygons
            string basePolygonFolder = this.baseFolder + "\\Polygon";
            if (!enableZipFolders)
            {
                Directory.CreateDirectory(basePolygonFolder);
            }

            string lookupPath = "";

            Dispatcher.Invoke(delegate()
            {
                lookupPath = this.lookupPathBox.Text;
            });
            
            if (IncludeColor && !useConnectedKinectForRegistration && (offlineDepthToRgbMapperX.Count == 0))
            {
                // Load the D2RGB lookup tables within a depth range of 500 to 8000 mm
                for (int i = 500; i < 8000; ++i)
                {
                    string lookupPathX = lookupPath + "\\Generic\\D2RGB-" + i.ToString("D5") + "-X.png";
                    string lookupPathY = lookupPath + "\\Generic\\D2RGB-" + i.ToString("D5") + "-Y.png";

                    ushort[] tmpMapX = new ushort[depthWidth * depthHeight];
                    ushort[] tmpMapY = new ushort[depthWidth * depthHeight];

                    Dispatcher.Invoke(delegate()
                    {
                        int progVal = i - 500;
                        this.progressBar.Value = (progVal * 100) / 7500;
                        this.StatusText = "Loading lookup table " + progVal.ToString() + " of " + 7500.ToString();
                    });

                    try
                    {
                        Emgu.CV.Image<Gray, UInt16> imgX = new Image<Gray, UInt16>(lookupPathX);
                        Emgu.CV.Image<Gray, UInt16> imgY = new Image<Gray, UInt16>(lookupPathY);

                        // Copy data from a 3D array into a 1D array
                        System.Buffer.BlockCopy(imgX.Data, 0, tmpMapX, 0, depthWidth * depthHeight * 2);
                        System.Buffer.BlockCopy(imgY.Data, 0, tmpMapY, 0, depthWidth * depthHeight * 2);

                        offlineDepthToRgbMapperX.Add(tmpMapX);
                        offlineDepthToRgbMapperY.Add(tmpMapY);
                    }
                    catch
                    {
                        StatusText = "Could not load " + lookupPath + "\\Generic\\D2RGB-" + i.ToString("D5");
                        return;
                    }
                }
            }

            if (!useConnectedKinectForRegistration)
            {
                // Load the lookup tables for the Depth to camera coordinates conversion
                offlineDepthToCameraMapper = new Microsoft.Kinect.PointF[depthWidth * depthHeight];

                string[] tmpXMap;
                string[] tmpYMap;

                try 
                {
                    tmpXMap = System.IO.File.ReadAllLines(lookupPath + "\\DepthFrameToCameraSpaceVectorX.txt");
                    tmpYMap = System.IO.File.ReadAllLines(lookupPath + "\\DepthFrameToCameraSpaceVectorY.txt");
                } 
                catch  
                {
                    try
                    {
                        tmpXMap = System.IO.File.ReadAllLines(lookupPath + "\\Generic\\DepthFrameToCameraSpaceVectorX.txt");
                        tmpYMap = System.IO.File.ReadAllLines(lookupPath + "\\Generic\\DepthFrameToCameraSpaceVectorY.txt");
                    }
                    catch
                    {
                        StatusText = "Could not load " + " DepthFrameToCameraSpaceVector";
                        return;
                    }
                }

                for (int i = 0; i < tmpXMap.Length; ++i)
                {
                    Microsoft.Kinect.PointF tmpPoint = new Microsoft.Kinect.PointF();
                    tmpPoint.X = float.Parse(tmpXMap[i]);
                    tmpPoint.Y = float.Parse(tmpYMap[i]);

                    offlineDepthToCameraMapper[i] = tmpPoint;
                }
            }


            for (int i = 0; i < depthFilePaths.Length; ++i)
            {
                // Read, write and convert the depth files
                
                // Decide if we need to read the color file. If we do, determine if the file exists
                bool colorReady;
                if (IncludeColor)
                {
                    if (readColorImage(colorBaseFolder, depthFilePaths[i]) == 0)
                    {
                        colorReady = true;
                    } else {
                        colorReady = false;
                    }
                } else {
                    colorReady = true;
                }

                if ((readDepthImage(depthFilePaths[i]) == 0) && colorReady)
                {
                    Dispatcher.Invoke(delegate()
                    {
                        int oneIndexed = i + 1;
                        this.progressBar.Value = (oneIndexed * 100) / depthFilePaths.Length;
                        this.StatusText = "Converting image " + oneIndexed.ToString() + " of " + depthFilePaths.Length.ToString();
                    });
                    
                    convertImageToPolygon();

                    // Save the .ply files in a seperate directory
                    string currentFile = depthFilePaths[i];
                    currentDepthFiles.Add(depthFilePaths[i]);

                    // If zip files are toggled off, the files should be saved individually
                    if (!enableZipFolders)
                    {
                        savePolygons(basePolygonFolder, currentFile);
                    }
                    else if ((currentDepthFiles.Count == batchSize) || (i == (depthFilePaths.Length - 1)))
                    {
                        // Otherwise, they are compressed in bunches of 20
                        saveCompressedPolygons(currentDepthFiles, i + 2 - batchSize, i + 1);
                        currentDepthFiles.Clear();
                    }

                    convertedImages++;
                }
            }

            Dispatcher.Invoke(delegate()
            {
                this.StatusText = "Converted " + convertedImages + " images";
                this.progressBar.Visibility = Visibility.Hidden;
            });
        }

        private void processDtoRgbRegistration()
        {
            // Get all .png-files in current directory (depth images)
            string[] depthFilePaths = Directory.GetFiles(this.baseFolder + "\\D", "*.png");
            string depthBaseFolder = this.baseFolder + "\\D\\";

            Dispatcher.Invoke(delegate()
            {
                this.progressBar.Visibility = Visibility.Visible;
            });

            // Create the registration folder
            string baseRegFolder = this.baseFolder + "\\Reg";
            Directory.CreateDirectory(baseRegFolder);

            int batchSize = 20;
            int convertedImages = 0;
            List<string> currentDepthFiles = new List<string>();

            string lookupPath = "";

            Dispatcher.Invoke(delegate()
            {
                lookupPath = this.regLookupPathBox.Text;
            });

            if (!useConnectedKinectForRegistration && (offlineRgbToDepthMapperX.Count == 0))
            {
                // Load the RGB2D lookup tables within a depth range of 500 to 8000 mm
                for (int i = 500; i < 8000; ++i)
                {
                    string lookupPathX = lookupPath + "\\Generic\\RGB2D-" + i.ToString("D5") + "-X.png";
                    string lookupPathY = lookupPath + "\\Generic\\RGB2D-" + i.ToString("D5") + "-Y.png";

                    ushort[] tmpMapX = new ushort[colorWidth * colorHeight];
                    ushort[] tmpMapY = new ushort[colorWidth * colorHeight];

                    Dispatcher.Invoke(delegate()
                    {
                        int progVal = i - 500;
                        this.progressBar.Value = (progVal * 100) / 7500;
                        this.StatusText = "Loading lookup table " + progVal.ToString() + " of " + 7500.ToString();
                    });

                    try
                    {
                        Emgu.CV.Image<Gray, UInt16> imgX = new Image<Gray, UInt16>(lookupPathX);
                        Emgu.CV.Image<Gray, UInt16> imgY = new Image<Gray, UInt16>(lookupPathY);

                        // Copy data from a 3D array into a 1D array
                        System.Buffer.BlockCopy(imgX.Data, 0, tmpMapX, 0, colorWidth * colorHeight * 2);
                        System.Buffer.BlockCopy(imgY.Data, 0, tmpMapY, 0, colorWidth * colorHeight * 2);

                        offlineRgbToDepthMapperX.Add(tmpMapX);
                        offlineRgbToDepthMapperY.Add(tmpMapY);
                    }
                    catch
                    {
                        StatusText = "Could not load " + lookupPath + "\\Generic\\RGB2D-" + i.ToString("D5");
                        return;
                    }
                }
            }

            for (int i = 0; i < depthFilePaths.Length; ++i)
            {
                // Read, write and convert the color files. Curiously, for the registration to work, the corresponding depth images must be loaded.
                if (readDepthImage(depthFilePaths[i]) == 0)
                {
                    Dispatcher.Invoke(delegate()
                    {
                        int oneIndexed = i + 1;
                        this.progressBar.Value = (oneIndexed * 100) / depthFilePaths.Length;
                        this.StatusText = "Converting image " + oneIndexed.ToString() + " of " + depthFilePaths.Length.ToString();
                    });

                    registerDMapInRgbSpace(baseRegFolder, depthFilePaths[i]);
                    convertedImages++;
                }
            }

            Dispatcher.Invoke(delegate()
            {
                this.StatusText = "Converted " + convertedImages + " images";
                this.progressBar.Visibility = Visibility.Hidden;
            });
        }

        private void registerDMapInRgbSpace(string baseDirectory, string currentFile)
        {
            // Provide an image with the dimensions of the color image, in the color map, that lists the corresponding depth of the color pixels at the exact positions.
            // This is done in the following steps:
            // Step 1: Convert the color frame to the depth space/depth frame to obtain depth coordinates for each color pixel
            // Step 2: For each pixel in the color frame, look up the corresponding depth value in the depth image by using the coordinates acquired in step 1
            // Step 3: Create an image with the same dimensions as the depth frame

            StringBuilder sb = new StringBuilder();
            int nbrPoints = 0;

            // Convert depth every depth pixel to world coordinates
            if (useConnectedKinectForRegistration)
            {
                this.coordinateMapper.MapColorFrameToDepthSpace(depthFrameData, depthPoints);
            }
            else
            {
                // Manually map the color frame to depth space
                int count = 0;
                foreach (DepthSpacePoint depthPoint in depthPoints)
                {
                    // Find the corresponding depth value at this position                    
                    int depthX = (int)Math.Floor(depthPoint.X + 0.5);
                    int depthY = (int)Math.Floor(depthPoint.Y + 0.5);
                    ushort pixelDepth = 0;

                    if ((depthX >= 0) && (depthX < depthWidth) && (depthY >= 0) && (depthY < depthHeight))
                    {
                        int depthIndex = (depthY * depthWidth) + depthX;
                        pixelDepth = depthFrameData[depthIndex];
                    }

                    if (pixelDepth >= 500 && pixelDepth < 10000)
                    {
                        depthPoints[count].X = offlineRgbToDepthMapperX[pixelDepth - 500][count];
                        depthPoints[count].Y = offlineRgbToDepthMapperY[pixelDepth - 500][count];
                    }
                    else
                    {
                        // We have an invalid point
                        depthPoints[count].X = Single.NegativeInfinity;
                        depthPoints[count].Y = Single.NegativeInfinity;
                    }
                    count++;
                }
            }

            // Loop thorugh every point to add it to the revised color/depth image
            ushort[] regDToRgbArray = new ushort[colorWidth * colorHeight];

            for (int i = 0; i < regDToRgbArray.Length; ++i)
            {
                DepthSpacePoint p = depthPoints[i];

                // We have to make sure that we map to a valid point in the depth space
                int depthX = (int)Math.Floor(p.X + 0.5);
                int depthY = (int)Math.Floor(p.Y + 0.5);
                ushort pixelDepth = 0;
                    
                if ((depthX >= 0) && (depthX < depthWidth) && (depthY >= 0) && (depthY < depthHeight))
                {
                    int depthIndex = (depthY * depthWidth) + depthX;
                    pixelDepth = depthFrameData[depthIndex];
                }

                // Write the corresponding depth in the registered image
                regDToRgbArray[i] = pixelDepth;
            }

            // Convert and save the image
            WriteableBitmap regDtoRgbBitmap = new WriteableBitmap(colorWidth, colorHeight, 96.0, 96.0, PixelFormats.Gray16, null);

            regDtoRgbBitmap.WritePixels(
                new Int32Rect(0, 0, colorWidth, colorHeight),
                regDToRgbArray,
                colorWidth * this.bytesPrDepthPixel,
                0);

            BitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(regDtoRgbBitmap));

            string filename = Path.GetFileName(currentFile.Replace(".png", "-reg.png"));
            string path = baseDirectory + "\\" + filename;

            using (FileStream fs = new FileStream(path, FileMode.Create))
                encoder.Save(fs);
        }
                

        private void browseButton_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog fDialog = new System.Windows.Forms.FolderBrowserDialog();
            fDialog.Description = "Choose base directory of D (and RGB) folders";

            fDialog.SelectedPath = this.pathBox.Text;
                
            DialogResult result = fDialog.ShowDialog();

            if (System.Windows.Forms.DialogResult.OK == result)
            {
                this.baseFolder = fDialog.SelectedPath;
                this.pathBox.Text = this.baseFolder;
            } 
        }

        public int SelectedTabIndex
        {
            get
            {
                return selectedTabIndex;
            }

            set
            {
                if (this.tabControl.SelectedIndex != value)
                {
                    this.selectedTabIndex = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("SelectedTabIndex"));
                    }
                }
            }
        }

        private void lookupPathBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            checkPath();
        }

        private void lookupBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog fDialog = new System.Windows.Forms.FolderBrowserDialog();
            fDialog.Description = "Choose base directory of Lookup tables";

            switch (SelectedTabIndex)
            { 
                case 0:
                    fDialog.SelectedPath = this.lookupPathBox.Text;
                    break;
                case 1:
                    fDialog.SelectedPath = this.regLookupPathBox.Text;
                    break;
            }

            DialogResult result = fDialog.ShowDialog();

            if (System.Windows.Forms.DialogResult.OK == result)
            {
                this.lookupBaseFolder = fDialog.SelectedPath;
                this.lookupPathBox.Text = this.lookupBaseFolder;
                this.regLookupPathBox.Text = this.lookupBaseFolder;                
            }
        }

        private async void convertButton_Click(object sender, RoutedEventArgs e)
        {
            enableControls(false);
            this.imageDirectory.IsEnabled = false;
            this.registrationProperties.IsEnabled = false;

            bool enableZipFolders = (bool)enableZipFoldersButton.IsChecked;
            await Task.Run(() => processImages(enableZipFolders));
            enableControls(true);
            this.imageDirectory.IsEnabled = true;
            this.registrationProperties.IsEnabled = true;
        }

        private void enableControls(bool state)
        {
            this.convertButton.IsEnabled = state;
            this.settingsGroupBox.IsEnabled = state;
        }

        private void IncludeColorButton_Triggered(object sender, RoutedEventArgs e)
        {
            IncludeColor = (sender as System.Windows.Controls.CheckBox).IsChecked.Value;
            checkPath();
        }


        private void pathBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            checkPath();
        }

        private void checkPath()
        {
            // Check if the exists depth (and optionally, color, lookup tables) files at the current directory
            int nbrDepthFiles = 0;
            int nbrColorFiles = 0;
            int nbrLookupFiles = 0;
            bool readyForConversion = false;
            string compactedString = "";
            string lookupCompactedString = "";

            switch (selectedTabIndex)
            {
                case 0: 
                    compactedString = GetCompactedString(this.pathBox.Text, 20);
                    lookupCompactedString = GetCompactedString(this.lookupPathBox.Text, 20);
                    break;
                case 1:
                    compactedString = GetCompactedString(this.regPathBox.Text, 20);
                    lookupCompactedString = GetCompactedString(this.regLookupPathBox.Text, 20);
                    break;
            }           

            string[] depthFilePaths = { "k" };

            try
            {
                switch (selectedTabIndex)
                {
                    case 0:
                        depthFilePaths = Directory.GetFiles(this.pathBox.Text + "\\D", "*.png");
                        break;
                    case 1:
                        depthFilePaths = Directory.GetFiles(this.regPathBox.Text + "\\D", "*.png");
                        break;
                }    
                
                nbrDepthFiles = depthFilePaths.Length;
            }
            catch (Exception)
            {
                this.StatusText = "Directory does not exist";
                nbrDepthFiles = 0;
            }

            this.depthFileStatusField.Text = nbrDepthFiles.ToString() + " depth images found at " + compactedString + "\\D";
            this.regDepthFileStatusField.Text = nbrDepthFiles.ToString() + " depth images found at " + compactedString + "\\D";

            if (IncludeColor)
            {
                string[] colorFilePaths = { "k" };

                try
                {
                    colorFilePaths = Directory.GetFiles(this.pathBox.Text + "\\RGB", "*.jpg");
                    nbrColorFiles = colorFilePaths.Length;
                }
                catch (Exception)
                {
                    this.StatusText = "Directory does not exist";
                    nbrColorFiles = 0;
                }

                this.colorFileStatusField.Text = nbrColorFiles.ToString() + " color images found at " + compactedString + "\\RGB";
                this.regColorFileStatusField.Text = nbrColorFiles.ToString() + " color images found at " + compactedString + "\\RGB";
            }
            else
            {
                this.colorFileStatusField.Text = " ";
            }

            if (IncludeColor)
            {
                // Number of files must be equal
                if ((nbrColorFiles > 0) && (nbrDepthFiles > 0))
                {
                    readyForConversion = true;
                }
                else
                {
                    this.StatusText = " ";
                }
            }
            else
            {
                if (nbrDepthFiles > 0)
                {
                    readyForConversion = true;
                }
                else
                {
                    this.StatusText = " ";
                }
            }

            if (!useConnectedKinectForRegistration)
            {
                // If we don't use the connected Kinect, we rely on offline lookup-files. Verify if these exist
                string[] lookupFilePaths = { "k" };

                try
                {
                    switch (selectedTabIndex)
                    {
                        case 0:
                            lookupFilePaths = Directory.GetFiles(this.lookupPathBox.Text + "\\Generic", "*.png");
                            break;
                        case 1:
                            lookupFilePaths = Directory.GetFiles(this.regLookupPathBox.Text + "\\Generic", "*.png");
                            break;
                    }
                                       
                    nbrLookupFiles = lookupFilePaths.Length;
                }
                catch (Exception)
                {
                    this.StatusText = "Lookup directory does not exist";
                    nbrLookupFiles = 0;
                }

                // There must a sufficient number of lookup tables for the conversion to work
                if (nbrLookupFiles > 15000)
                {
                    this.lookupFileStatusField.Text = "Lookup tables found at " + lookupCompactedString + "\\Generic";
                    this.regLookupFileStatusField.Text = "Lookup tables found at " + lookupCompactedString + "\\Generic";
                }
                else
                {
                    this.lookupFileStatusField.Text = "No lookup tables found at " + lookupCompactedString + "\\Generic";
                    this.regLookupFileStatusField.Text = "No lookup tables found at " + lookupCompactedString + "\\Generic";
                    readyForConversion = false;
                }
            }
            else
            {
                this.lookupFileStatusField.Text = "Lookup tables provided by connected Kinect sensor";
                this.regLookupFileStatusField.Text = "Lookup tables provided by connected Kinect sensor";

            }

            if (readyForConversion)
            {
                
                this.StatusText = "Ready for conversion";
                enableControls(true);

                switch (selectedTabIndex)
                {
                    case 0:
                        this.convertButton.IsEnabled = true;
                        if (pathBox.Text.EndsWith("\\"))
                        {
                            pathBox.Text.Remove(pathBox.Text.Length - 2);
                        }

                        this.baseFolder = pathBox.Text;
                        break;
                    case 1:
                        this.regConvertButton.IsEnabled = true;
                        if (regPathBox.Text.EndsWith("\\"))
                        {
                            regPathBox.Text.Remove(pathBox.Text.Length - 2);
                        }

                        this.baseFolder = regPathBox.Text;
                        break;
                }
                

                
            }
            else
            {
                enableControls(false);
            }
        }

        private string GetCompactedString(string stringToCompact, int maxLength)
        {
            // Thanks to StackOverflow; http://stackoverflow.com/questions/1764204/how-to-display-abbreviated-path-names-in-net

            if (stringToCompact.Length > (maxLength+10))
            {
                int length = stringToCompact.Length;
                int startPos = stringToCompact.Length - maxLength;

                string compactedString = stringToCompact.Substring(0,3) + "..." + stringToCompact.Substring(startPos);

                return compactedString;
            }
            else
            {
                return stringToCompact;
            }
        }

        private void useConnectedKinect_Click(object sender, RoutedEventArgs e)
        {
            regUseConnectedKinect.IsChecked = true;
            useConnectedKinect_Click();
        }

        private void regUseConnectedKinect_Click(object sender, RoutedEventArgs e)
        {
            useConnectedKinect.IsChecked = true;
            useConnectedKinect_Click();
        }

        private void useConnectedKinect_Click()
        {
            bool isOpen = openKinectSensor();
            
            if (isOpen)
            {
                this.lookupBrowseButton.IsEnabled = false;
                this.regLookupBrowseButton.IsEnabled = false;
                this.lookupPathBox.IsEnabled = false;
                this.regLookupPathBox.IsEnabled = false;
                useConnectedKinectForRegistration = true;
                checkPath();
            }
            else
            {
                useOfflineRegistration.IsChecked = true;
                this.regUseOfflineRegistration.IsChecked = true;
                useOfflineRegistration_Click();
            }
        }
        
        private void useOfflineRegistration_Click(object sender, RoutedEventArgs e)
        {
            regUseOfflineRegistration.IsChecked = true;
            useOfflineRegistration_Click();
        }

        private void regUseOfflineRegistration_Click(object sender, RoutedEventArgs e)
        {
            useOfflineRegistration.IsChecked = true;
            useOfflineRegistration_Click();
        }

        private void useOfflineRegistration_Click()
        {
            useOfflineRegistration.IsChecked = true;
            regUseOfflineRegistration.IsChecked = true;
            
            if (useConnectedKinectForRegistration)
            {
                // Close the sensor if it is connected
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }

            this.lookupBrowseButton.IsEnabled = true;
            this.regLookupBrowseButton.IsEnabled = true;
            this.lookupPathBox.IsEnabled = true;
            this.regLookupPathBox.IsEnabled = true;
            useConnectedKinectForRegistration = false;


            checkPath();
        }

        private async void regConvertButton_Click(object sender, RoutedEventArgs e)
        {
            enableControls(false);
            this.regImageDirectory.IsEnabled = false;
            this.regRegistrationProperties.IsEnabled = false;

            await Task.Run(() => processDtoRgbRegistration());
            enableControls(true);
            this.regImageDirectory.IsEnabled = true;
            this.regRegistrationProperties.IsEnabled = true;
        }
    }

          
    

  
}
